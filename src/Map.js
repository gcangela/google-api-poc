import React, { useState } from 'react';
import { initMap } from './mapService';
import { loadMapsScript } from './loadMapScript';



const Map = () => {
  const [placeDetails, setPlaceDetails] = useState({});
  React.useEffect(() => {
   loadMapsScript(() => {
    window.google.maps && initMap(setPlaceDetails, window);
   })
  }, []);

  return <div style={{height: 400, width:'100%'}} id="map" />;
};

export default Map;
