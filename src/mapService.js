export function initMap(setPlaceDetails, window) {
  var map = new window.google.maps.Map(document.getElementById('map'), {
    mapTypeControl: false,
    center: { lat: 12.510052, lng: -70.009354 },
    zoom: 12,
  });
  setClickHandler(map, setPlaceDetails);

  new AutocompleteHandler(map, setPlaceDetails);
}

export function AutocompleteHandler(map, setPlaceDetails) {
  this.map = map;
  this.setPlaceDetails = setPlaceDetails;

  var originInput = document.getElementById('searchPlaces');

  var originAutocomplete = new window.google.maps.places.Autocomplete(originInput);
  originAutocomplete.setFields(['place_id', 'adr_address', 'geometry', 'formatted_address', 'name']);

  this.setupPlaceChangedListener(originAutocomplete, setPlaceDetails);
}

AutocompleteHandler.prototype.setupPlaceChangedListener = function(autocomplete, setPlaceDetails) {
  var me = this;
  autocomplete.addListener('place_changed', function() {
    const place = autocomplete.getPlace();

    setPlaceDetails(place);
    const coordinates = { lat: place.geometry.location.lat(), lng: place.geometry.location.lng() };
    me.map.panTo(coordinates);
    // Initialize custom infowindow and populate with address from the placeId
    const infoWindow = new window.google.maps.InfoWindow({
      position: coordinates,
      content: `<div style="color:black;"><h2>${place.name}</h2></br><p>${place.adr_address}</p></div>`,
    });
    infoWindow.open(me.map);
    me.map.setZoom(18);
    if (!place.place_id) {
      window.alert('Please select an option from the dropdown list.');
      return;
    }
  });
};

function setClickHandler(map, setPlaceDetails) {
  const placesServices = new window.google.maps.places.PlacesService(map);
  map.addListener('click', event => {
    placesServices.getDetails({ placeId: event.placeId }, result => {
      setPlaceDetails(result);
    });
    if (!event.placeId) {
      return;
    }

    map.panTo(event.latLng);
  });
}
