import React from 'react';
import './App.css';
import Map from './Map';

function App() {
  return (
    <div className="App">
      <header className="App-header">
       <Map />
      <input style={{width: '300px'}}id="searchPlaces"/>
      </header>
    </div>
  );
}

export default App;
