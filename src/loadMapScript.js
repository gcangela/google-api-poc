import { apiKey } from "./config";

const key = `https://maps.googleapis.com/maps/api/js?key=${apiKey}&libraries=places`

export async function loadMapsScript(callback) {
  var googleMapsApiScript = document.createElement('script');
  await googleMapsApiScript.setAttribute('src', key);
  await googleMapsApiScript.setAttribute('async', '');
  await  googleMapsApiScript.setAttribute('defer','');
  document.body.append(googleMapsApiScript)
  setTimeout(() => {
    callback()
  }, 200);
 }